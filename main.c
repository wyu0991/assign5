#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "main.h"
struct Thirdarray {
    double *firstarray;
    double *secondarray;
};
int size = 0;

int main(int argc, char *argv[]) {
    struct timespec ts_begin, ts_end;
    struct Thirdarray *thirdarray=malloc(sizeof(struct Thirdarray));
    double elapsed;
    int n = atoi(argv[1]);
    time_t t;
    double array[n];
    srand((unsigned) time(&t));
    int i;
    int z;
    pthread_t tid;
    void *status;
    void *status2;
    void *status3;
    void *status4;
    for (i = 0; i < n; i++) {
        double x = rand();
        if (x < 1 || x > 1000) {
            i--;
            continue;
        }
        array[i] = x;
        size += 1;
    }
    double barray[n];
    for (i = 0; i < size; i++) {
        barray[i] = array[i];
    }
//    clock_gettime(CLOCK_MONOTONIC, &ts_begin);
//    pthread_create(&tid, NULL, selectionSort, (void *) barray);
//    pthread_join(tid, &status);
//    clock_gettime(CLOCK_MONOTONIC, &ts_begin);
//    clock_gettime(CLOCK_MONOTONIC, &ts_end);
//    elapsed = ts_end.tv_sec - ts_begin.tv_sec;
//    elapsed += (ts_end.tv_nsec - ts_begin.tv_nsec) / 1000000000.0;
//    printf("%lf\n", elapsed);
    double *nums = (double *) status;
    int halfs = size / 2;
    double first_half_array[halfs];
    double second_half_array[halfs];
    for (i = 0; i < halfs; i++) {
        first_half_array[i] = array[i];
    }
    for (z = 0, i = halfs; i < size; i++, z++) {
        second_half_array[z] = array[i];
    }
    size = size - halfs;
    pthread_t tid2;
    pthread_t tid3;
    clock_gettime(CLOCK_MONOTONIC, &ts_begin);
    //pthread_create(&tid2, NULL, selectionSort, (void *) first_half_array);
    //pthread_create(&tid3, NULL, selectionSort, (void *) second_half_array);
    //pthread_join(tid2, &status2);
    //pthread_join(tid3, &status3);
    double *nums2 = (double *)selectionSort((void *)first_half_array);
    double *nums3 = (double *)selectionSort((void *)second_half_array);
    //thirdarray.secondarray = malloc(sizeof(double) * halfs);
    //memset(thirdarray.firstarray, 0, sizeof(double)*halfs);
    //memset(thirdarray.secondarray, 0, sizeof(double)*halfs);
    thirdarray->firstarray = nums2;
    thirdarray->secondarray = nums3;
     mergeSort((void *)thirdarray);
//    pthread_t tid4;
//    pthread_create(&tid4, NULL, mergeSort, (void *) thirdarray);
//    pthread_join(tid4, &status4);
}

void *selectionSort(void *arg) {
    double *nums = (double *) arg;
    int i;
    int min = 0;
    int z;
    double current;
    int mins = 0;
    printf("Before Array\n");
    for (i = 0; i < size; i++) {
        printf("%lf\n", nums[i]);
    }
    for (z = 0; z < size; z++) {
        min = z;

        for (i = z; i < size; i++) {

            if (nums[min] > nums[i]) {
                min = i;
            }
        }

        current = nums[z];
        nums[z] = nums[min];
        nums[min] = current;
    }
    printf("Sorted Array\n");
    for (i = 0; i < size; i++) {
        printf("%lf\n", nums[i]);
    }
    return (void *) nums;
}

void *mergeSort(void *arg) {
    struct Thirdarray *thirdarray = (struct Thirdarray*) arg;
    double mergedarray[6];
    int i;
    int d;
    int z;
    int min, min2;
    int mt = 0;
    //seomthing wrong with insert into the merged looks like a small problem debug. 
    printf("first array of merge sort %d\n", size);
      for (i = 0; i < size; i++) {
        printf("%lf\n", (*thirdarray).firstarray[i]);
    }
     printf("second array of merge sort %d\n", size);
         for (i = 0; i < size; i++) {
        printf("%lf\n", (*thirdarray).secondarray[i]);
    }
    for (z = 0; z < size + size; z++) {
        //printf("HELLOOO\n");
        while (mt<size-1&&(*thirdarray).firstarray[mt] == -1) {
            if (mt >= size)
                break;
            mt++;
        }
        min = mt;
        for (i = 0; i < size; i++) {
            if ((*thirdarray).firstarray[min] > (*thirdarray).firstarray[i]&&(*thirdarray).firstarray[i] != -1) {
                min = i;
            }
        }
        mt = 0;
        while (mt<size-1&&(*thirdarray).secondarray[mt] == -1) {
            if (mt >= size)
                break;
            mt++;
        }
         min2 = mt;
        for (d = 0; d < size; d++) {
            if ((*thirdarray).secondarray[min2] > (*thirdarray).secondarray[d]&&(*thirdarray).secondarray[d] != -1) {
                min2 = d;
            }
        }
        if ((*thirdarray).secondarray[min2] == -1 && (*thirdarray).firstarray[min] != -1) {
            mergedarray[z] = (*thirdarray).firstarray[min];
            (*thirdarray).firstarray[min] = -1;
            continue;
        }
        if ((*thirdarray).firstarray[min] == -1 && (*thirdarray).secondarray[min2] != -1) {
            mergedarray[z] = (*thirdarray).secondarray[min2];
            (*thirdarray).secondarray[min2] = -1;
            continue;
        }
        if ((*thirdarray).secondarray[min2] > (*thirdarray).firstarray[min]) {
            mergedarray[z] = (*thirdarray).firstarray[min];
            (*thirdarray).firstarray[min] = -1;
        }
        if ((*thirdarray).secondarray[min2] < (*thirdarray).firstarray[min]) {
            mergedarray[z] = (*thirdarray).secondarray[min2];
            (*thirdarray).secondarray[min2] = -1;
        }
    }
    printf("END of merge sort \n");
    for (i = 0; i < size+size; i++) {
        printf("%lf\n", mergedarray[i]);
    }
    return (void *)mergedarray;
}

